package main;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BoardPanel extends JPanel {
	
	GridLabel[][] labels;
	
	
	public BoardPanel(){
		
		init();
		
	}
	
	private void init(){
		
		setPreferredSize(new Dimension(600,450));
		setMaximumSize(new Dimension(600,450));
		setMinimumSize(new Dimension(600,450));
		setLayout(new GridLayout(9, 12, 0, 0));
		
		labels = new GridLabel[9][12];
		
		for (int i = 0; i<9; i++){
			
			for (int j = 0; j < 12; j++){
				
				GridLabel b = new GridLabel(j,i);
				labels[i][j] = b;
				add(b);
				
			}

		}

		setVisible(true);
			
	}
	
	
	public void setOccupied(int x, int y){
		
		labels[y][x].setOccupied();
		revalidate();
		repaint();
		
	}
	
	public boolean isOccupied(int x, int y){
		
		return labels[y][x].isOccupied();
		
	}

}
