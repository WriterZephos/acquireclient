package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class GridButton extends JButton {

	GridButton[][] buttonGroup;
	Tile tile;
	Border border = new LineBorder(Color.BLUE);
	
	public GridButton(Tile t){
		tile = t;
		setFont(new Font("Arial", Font.PLAIN, 8));
		setText("" + t.getX() + ", " + t.getY());
		
		setMinimumSize(new Dimension(50,50));
		setPreferredSize(new Dimension(50,50));
		setMinimumSize(new Dimension(50,50));
		setMargin(new Insets(0,0,0,0));
		setVisible(true);
	}
	
	
	public Tile getTile(){
		
		return tile;
	}
	
	
	
}
