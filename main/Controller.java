package main;

import java.util.HashMap;

import javax.swing.SwingUtilities;

import messages.JSONMaker;
import messages.MessageFactory;
import messages.MessageParser;
import messages.MessageFactory.Company;
import messages.MessageParser.Update;

import org.json.JSONObject;

public class Controller{
	
	Application app;

	
	public Controller(Application app){
		
		this.app = app;
	}

	
	public void handle(String jsonString){
		
		JSONObject jObject = new JSONObject(jsonString);
		
		
		if(jObject.has("type") && jObject.get("type") != null && jObject.has("message") 
				&& jObject.get("message") != null){
			
		
			String type = jObject.getString("type");
			
			if (type.equals("chat")  && jObject.has("fromUser") && jObject.get("fromUser") != null){
				
				//display global chat here - this is handled in different places because people are formatting their strings weird
				SwingUtilities.invokeLater(() ->app.getView().setChatText(jObject.getString("fromUser") + ": " + jObject.getString("message")));	
			
			}  else if(type.equals("acknowledge")){
				
				//handle acknowledge message here
				SwingUtilities.invokeLater(() ->app.getView().setChatText("Server: " + jObject.getString("message")));
				
			} else if(type.equals("application")){
				
				if(jObject.has("module") && jObject.get("module") != null && jObject.getString("module").equals("acquire")){
					
					
					if(jObject.has("gameAction") && jObject.get("gameAction") != null){
						
						String gameAction = jObject.getString("gameAction");
						
						if(gameAction.equals("GAME")){
							
							if(jObject.has("option") && jObject.get("option") != null 
									&& jObject.has("request") && jObject.get("request") != null){
							
								String option = jObject.getString("option");
								String request = jObject.getString("request");
							
								if(option.equals("CONFIRM")){
										
									SwingUtilities.invokeLater(() ->app.getView().setChatText("Action confirmed by server."));
									
									switch(request){
									
										case "JOIN":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("You joined the game."));
											break;
										case "STARTGAME":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("You started the game."));
											break;
										case "ENDGAME":
											break;
										case "BUY":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("You purchased stock (wait for it to appear in the total)."));
											break;
										case "TRADE":
											break;
										case "SELL":
											break;
										case "TILEPLACED":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("You placed a tile."));
											break;
									}
									

								} else if(option.equals("DENY")){
									
									SwingUtilities.invokeLater(() ->app.getView().setChatText("Action denied by server."));
									
									switch(request){
									
										case "JOIN":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("Could not join the game."));
											break;
										case "STARTGAME":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("Could not start the game."));
											break;
										case "ENDGAME":
											break;
										case "BUY":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("Could not buy stock."));
											break;
										case "TRADE":
											break;
										case "SELL":
											break;
										case "TILEPLACED":
											SwingUtilities.invokeLater(() ->app.getView().setChatText("Could not place tile."));
											break;
									}	
								} 
							} 
						}
					} 
				} else {
						
					
					JSONObject o = jObject.getJSONObject("message");
					
					if(o.has("gameAction") && o.get("gameAction") != null
							&& o.has("option") && o.get("option") != null
							&& o.has("module") && o.get("module") != null 
							&& o.getString("module").equals("acquire")){
						
						
						if(o.getString("option").equals("UPDATE") && o.getString("gameAction").equals("GAME")){
							
							Update update = MessageParser.parseUpdate(jObject);
							
							if(update.getPlayersMap().get(app.getUsername()) != null){
							
								HashMap<String,String> me = update.getPlayersMap().get(app.getUsername());	
								
								String tileString = me.get("tiles");
								String[] tokens = tileString.split("-");
								
								app.getView().resetTSP();
								
								if(!tileString.isEmpty()){
									
									for(String s : tokens){
										
										//split (pipe is a special character so it is escaped)
										String[] coordinates = s.split("\\|");
										Tile t = new Tile(Integer.parseInt(coordinates[0]),Integer.parseInt(coordinates[1]));
										//add to hand
										app.getView().addTileToTSP(t);
										
									}
								}
								
								Company[] array = Company.values();
								
								for(int i = 0; i < 7 ; i++){
									Company c = array[i];
									String s = c.toString();
									
									String stock = me.get(s);
									app.getView().setLabel(i, stock);
									
								}
								
								app.getView().setMoney(me.get("money"));
								
								
								String board = update.getBoard();
								
								char[] b = board.toCharArray();
								
								for(int i = 0 ; i < 108 ; i++){
									if(b[i] != '0'){
										
										int x = i % 12;
										int y;
										if(i < 12){
											y = 0;
										} else {
											y = (i - x)/12;
										}
						
										app.getView().getBoard().setOccupied(x,y);
									}
									
									
									
								}
								
								
		
							}
							
							
						}
						
						//update gui client here using fields in the Update class.
	
					}
				}
	
			}
			
		}  

	} 
	
	
	public boolean buyStock(MessageFactory.Company c, int amount){
			
			try {
				app.getOutQ().put(MessageFactory.createStockMessage(
						MessageFactory.StockType.BUY, c, amount, Application.username));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				return false;
			}
			
			return true;			
	}
	
	
	public boolean startGame(){
		
		try {
			app.getOutQ().put(MessageFactory.createStartGameMessage(Application.username));
	
		} catch (InterruptedException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return true;
	
	}
	
	
	public boolean joinGame(){
		
		try {
			app.getOutQ().put(MessageFactory.createJoinMessage(Application.username));
	
		} catch (InterruptedException e) {
			
			e.printStackTrace();
			return false;
		}
		
		return true;
	
	}
	
	
	public boolean placeTile(Tile t){
		
		try {
			app.getOutQ().put(MessageFactory.createTilePlacedMessage(t, Application.username));
		} catch (InterruptedException e) {
			return false;
		}
		
		return true;			
	}
	
	public boolean sendChat(String chat){
		
		try {
			app.getOutQ().put(JSONMaker.makeGlobalChatMessage(chat));
		} catch (InterruptedException e) {
			return false;
		}
		
		return true;			
	}
	
	
} // class
