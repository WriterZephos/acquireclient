package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

import messages.MessageFactory;
import messages.MessageFactory.Company;

@SuppressWarnings("serial")
public class ControlPanel extends JPanel {
	
	Application app;
	JButton buyButton;
	JButton startGame;
	JButton joinGame;
	JSpinner spinner;
	JComboBox<MessageFactory.Company> companies;
	Company[] options;

	
	public ControlPanel(Application app){
		
		this.app = app;
		options = new Company[7];
		
		init();
		
	}

	
	public void init(){
		
		options = new Company[7];
		
		for(int i = 0; i < 7 ; i++){
			
			options[i] = MessageFactory.Company.values()[i];
			
		}
		
		setLayout(new GridLayout(0,2));
		setPreferredSize(new Dimension(220,200));
		setMaximumSize(new Dimension(220,200));
		setMinimumSize(new Dimension(220,200));
		
		joinGame = new JButton("Join Game");
		joinGame.setAlignmentX(CENTER_ALIGNMENT);
		joinGame.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				app.getController().joinGame();
	
			}});
		
		
		startGame = new JButton("Start Game");
		startGame.setAlignmentX(CENTER_ALIGNMENT);
		startGame.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				app.getController().startGame();
	
			}});
		
		
		companies = new JComboBox<>(options);
		SpinnerModel model = new SpinnerNumberModel(1,1,3,1); 
		spinner = new JSpinner(model);
		JComponent[] array = {companies,spinner};
		buyButton = new JButton("Buy Stock");
		buyButton.setAlignmentX(CENTER_ALIGNMENT);
		buyButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				int input = JOptionPane.showConfirmDialog(null,array,"Purchas Stock",JOptionPane.OK_CANCEL_OPTION);
				
				if(input == JOptionPane.OK_OPTION){
					
					app.getController().buyStock((MessageFactory.Company) companies.getSelectedItem(), (Integer) spinner.getValue());
					
				}
	
			}});
		
		add(joinGame);
		add(startGame);
		add(buyButton);
		setBackground(Color.RED);
		setOpaque(true);
		setVisible(true);
		
	}
	
	
}
