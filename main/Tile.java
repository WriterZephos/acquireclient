package main;

import messages.MessageFactory;

public class Tile implements Comparable<Tile>
{

	int x;
	int y;
	
	MessageFactory.Company company;
	
	public Tile()
	{
		this.x = -1;
		this.y = -1;
		this.company = MessageFactory.Company.BLANK;
		
	}
	
	public Tile(int x, int y){
		
		this.x = x;
		this.y = y;
		this.company = MessageFactory.Company.NOT_SET;
	
	}

	public MessageFactory.Company getCompany() {
		return company;
	}

	public void setCompany(MessageFactory.Company company) {
		this.company = company;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	@Override
	public int compareTo(Tile tile) {
		if(this.getX() == tile.getY() && this.getY() == tile.getY())
		{
			return 0;
		}
		else{return 1;}
	}
	
	@Override
	public String toString()
	{
		return String.format("%d|%d",this.x,this.y);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((company == null) ? 0 : company.hashCode());
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass()){
			return false;
		}
			
		Tile other = (Tile) obj;
		if (company != other.company)
			return false;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	
	

}
