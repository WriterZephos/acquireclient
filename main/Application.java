package main;


import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedTransferQueue;

import messages.JSONMaker;
import messages.MessageFactory;
import messages.MessageReceiver;
import messages.MessageSender;

import org.json.JSONException;
import org.json.JSONObject;

public class Application implements Runnable{
	
	private View view;
	private Socket socket;
    private Thread connectionOut;
    private Thread connectionIn;
    private Thread applicationThread;
    private BlockingQueue<JSONObject> outputQ;
    private BlockingQueue<String> inputQ;
    private String ip;
    private Controller logic;
    public final static String username = "bryant-" + Math.random();
	
	public Application(){
		init();
       
	}
    
	public Application(String ip){
		this.ip = ip;
		init();
       
	}
	
	
	private void init(){
		
		applicationThread = new Thread(this);
		view = new View(this);
		logic = new Controller(this);
		outputQ = new LinkedTransferQueue<>();
        inputQ = new LinkedTransferQueue<>();
		try {
            socket = ip == null ? new Socket("ec2-52-43-242-239.us-west-2.compute.amazonaws.com", 8989) : new Socket(ip,8989);
        } catch (IOException e) {
            e.printStackTrace();
        }


        //initiallizing the output thread.
        connectionOut = new Thread(new MessageSender(socket, outputQ));

        //starting the output thread.
        connectionOut.start();

        //initiallize the input thread
        connectionIn = new Thread(new MessageReceiver(socket, inputQ));

        //start the input thread
        connectionIn.start();
		
		
		applicationThread.start();
		
	}



	@Override
	public void run() {
		
		
		try {
			outputQ.put(JSONMaker.makeLoginMessage(username));
			outputQ.put(MessageFactory.createTestMessage(Application.username));
		} catch (JSONException | InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		while(!Thread.currentThread().isInterrupted()){
			
			String message = null;
			
			try {
				message = inputQ.take();
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				break;
			}
			
			
			
			//client side game logic
			logic.handle(message);
			
		}
	
		cleanup();
		System.exit(0);
	
	}
	
	public void cleanup(){
		
		connectionOut.interrupt();
        connectionIn.interrupt();
        try {
			socket.close();
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	public BlockingQueue<JSONObject> getOutQ(){
		
		return outputQ;
	}
	
	public BlockingQueue<String> getInQ(){
		
		return inputQ;
	}

	public Thread getConnectionOut() {
		return connectionOut;
	}

	public Thread getConnectionIn() {
		return connectionIn;
	}
	
	public Thread getApplicationThread() {
		return applicationThread;
	}

	public View getView() {
		// TODO Auto-generated method stub
		return view;
	}
	
	public Controller getController(){
		
		return logic;
		
	}
	
	public String getUsername(){
		
		return username;
		
	}
	
	
}
