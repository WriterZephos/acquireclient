package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class View{
	
	JFrame frame;
	JPanel main;
	BoardPanel board;
	ChatPanel chatPanel;
	JTextArea chatBox;
	TileSetPanel tsp;
	InfoPanel info;
	ControlPanel cp;
	
	Application app;

	public View(Application app){
		this.app = app;
		
		init();
	}
	

	private void init(){
		
		frame = new JFrame();
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        
		       app.getApplicationThread().interrupt();
		    }
		});
		
		main = new JPanel();
		main.setLayout(new BorderLayout());
		main.setPreferredSize(new Dimension(1120,650));
		main.setMaximumSize(new Dimension(1120,650));
		main.setMinimumSize(new Dimension(1120,650));
		
		board = new BoardPanel();
		chatPanel = new ChatPanel(app);
		info = new InfoPanel();
		tsp = new TileSetPanel(board,app);
		cp = new ControlPanel(app);
		
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());
		p.setPreferredSize(new Dimension(300,200));
		p.setMaximumSize(new Dimension(300,200));
		p.setMinimumSize(new Dimension(300,200));
		p.setBackground(Color.BLUE);
		p.setOpaque(true);
		p.setVisible(true);
		
		p.add(tsp,BorderLayout.CENTER);
		p.add(cp,BorderLayout.EAST);
		
		main.add(board,BorderLayout.WEST);
		main.add(chatPanel, BorderLayout.EAST);
		main.add(p, BorderLayout.SOUTH);
		main.add(info,BorderLayout.CENTER);

		frame.setContentPane(main);
		frame.pack();
		frame.setResizable(false);
		frame.setVisible(true);
		frame.revalidate();
		frame.repaint();
	}



	public void setChatText(String chat) {
		
		chatPanel.addMessage(chat);
		frame.revalidate();
		frame.repaint();
	}
	
	
	public void resetTSP(){
		
		tsp.reset();
		
		
	}
	
	
	public void addTileToTSP(Tile t){
		
		tsp.addTile(t);
		
		
	}
	
	
	public void setLabel(int i, String s){
		
		info.setLabel(i, s);
	}


	public void setMoney(String string) {
		info.setMoney(string);
		
	}


	public BoardPanel getBoard() {
		return board;
		
	}
	
}
