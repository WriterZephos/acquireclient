package main;

import java.util.HashMap;

public class Update{
	
	
	HashMap<String,HashMap<String,String>> playersMap;
	String board;
	
	public Update(HashMap<String,HashMap<String,String>> playersMap,String board){
		this.playersMap = playersMap;
		this.board = board;
		
	}

	public HashMap<String, HashMap<String, String>> getPlayersMap() {
		return playersMap;
	}

	public String getBoard() {
		return board;
	}

	
}
