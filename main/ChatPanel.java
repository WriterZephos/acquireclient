package main;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ChatPanel extends JPanel {
	
	JScrollPane scroll;
	JTextArea area;
	JTextField field;
	JButton sendButton;
	Application app;

	
	public ChatPanel(Application app){
		
		this.app = app;
		
		setPreferredSize(new Dimension(320,650));
		setMaximumSize(new Dimension(320,650));
		setMinimumSize(new Dimension(320,650));
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));

		
		area = new JTextArea();
		area.setLineWrap(true);
		scroll = new JScrollPane(area);
		scroll.setPreferredSize(new Dimension(300,300));
		scroll.setMaximumSize(new Dimension(300,300));
		scroll.setMinimumSize(new Dimension(300,300));
		
		
		field = new JTextField();
		field.setAlignmentY(CENTER_ALIGNMENT);
		field.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				
				app.getController().sendChat(field.getText());
				addMessage("You: " + field.getText());
				field.setText("");
				
			}});
		
		
		sendButton = new JButton("Send");
		sendButton.setAlignmentY(CENTER_ALIGNMENT);
		
		sendButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				app.getController().sendChat(field.getText());
				addMessage("You: " + field.getText());
				field.setText("");
				
			}});
		
		
		
		
		
		JPanel containerPanel = new JPanel();
		containerPanel.setPreferredSize(new Dimension(300,40));
		containerPanel.setMaximumSize(new Dimension(300,40));
		containerPanel.setMinimumSize(new Dimension(300,40));
		containerPanel.setLayout(new BoxLayout(containerPanel,BoxLayout.LINE_AXIS));
		
		containerPanel.add(field);
		containerPanel.add(Box.createRigidArea(new Dimension(5,40)));
		containerPanel.add(sendButton);
		containerPanel.setVisible(true);
		
		addComponent(scroll);
		add(Box.createRigidArea(new Dimension(190,10)));
		addComponent(containerPanel);
		setVisible(true);
		
	}
	
	private void addComponent(JComponent c){
		
		c.setAlignmentX(CENTER_ALIGNMENT);
		add(c);
		add(Box.createRigidArea(new Dimension(190,30)));
	}
	
	public void addMessage(String message){
		
		area.setText(area.getText() + "\n" + message);
		revalidate();
		repaint();
		
		
	}
	
	
	
	
}
