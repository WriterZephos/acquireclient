package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;


@SuppressWarnings("serial")
public class GridLabel extends JLabel{
	
	ImageIcon defaultImage;
	GridButton[][] buttonGroup;
	boolean occupied = false;;
	Border border = new LineBorder(Color.BLUE);
	
	public GridLabel(int x, int y){
		
		setFont(new Font("Arial", Font.PLAIN, 8));
		setText("" + x + ", " + y);
		setBackground(Color.WHITE);
		setOpaque(true);
		setHorizontalAlignment(SwingConstants.CENTER);
		
		setMinimumSize(new Dimension(50,50));
		setPreferredSize(new Dimension(50,50));
		setMinimumSize(new Dimension(50,50));
		setOpaque(true);
		setVisible(true);
		addMouseListener(new mouseListener());
		
	}
	
	public void setOccupied(){
		
		occupied = true;
		setBackground(Color.GRAY);
		
	}
	
	public boolean isOccupied(){
		return occupied;
	}
	
	private class mouseListener extends MouseAdapter{

		@Override
		public void mouseEntered(MouseEvent e) {

			setBorder(border);
			repaint();
		}
	
		@Override
		public void mouseExited(MouseEvent e) {
			

			setBorder(null);
			repaint();
		}

	}

}
