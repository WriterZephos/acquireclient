package main;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import messages.MessageFactory.Company;

@SuppressWarnings("serial")
public class InfoPanel extends JPanel {
	
	JLabel money;
	JLabel stockLabels[];

	public InfoPanel(){
		
		init();
		
	}
	
	private void init(){
		
		setPreferredSize(new Dimension(200,450));
		setMaximumSize(new Dimension(200,450));
		setMinimumSize(new Dimension(200,450));
		setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
		stockLabels = new JLabel[7];
		money = new JLabel("" + 00.00d);
		
		stockLabels[0] = new JLabel("" + 0);
		stockLabels[0].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels = new JLabel[7];
		
		stockLabels[0] = new JLabel("" + 0);
		stockLabels[0].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels[1] = new JLabel("" + 0);
		stockLabels[1].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels[2] = new JLabel("" + 0);
		stockLabels[2].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels[3] = new JLabel("" + 0);
		stockLabels[3].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels[4] = new JLabel("" + 0);
		stockLabels[4].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels[5] = new JLabel("" + 0);
		stockLabels[5].setAlignmentY(CENTER_ALIGNMENT);
		
		stockLabels[6] = new JLabel("" + 0);
		stockLabels[6].setAlignmentY(CENTER_ALIGNMENT);
		
		JPanel container = new JPanel();
		container.setPreferredSize(new Dimension(200,40));
		container.setMaximumSize(new Dimension(200,40));
		container.setMinimumSize(new Dimension(200,40));
		container.setAlignmentX(CENTER_ALIGNMENT);
		JLabel l = new JLabel(" Money: ");
		l.setPreferredSize(new Dimension(120,40));
		l.setMaximumSize(new Dimension(120,40));
		l.setMinimumSize(new Dimension(120,40));
		l.setHorizontalTextPosition(SwingConstants.LEFT);
		container.add(l);
		container.add(money);
		container.setVisible(true);
		add(container);
		add(Box.createRigidArea(new Dimension(200,15)));
		
		
		Company[] array = Company.values();
		
		for(int i = 0; i < 7 ; i++){
			Company c = array[i];
			
			container = new JPanel();
			container.setPreferredSize(new Dimension(200,40));
			container.setMaximumSize(new Dimension(200,40));
			container.setMinimumSize(new Dimension(200,40));
			container.setAlignmentX(CENTER_ALIGNMENT);
			l = new JLabel(c.toString() + ": ");
			l.setPreferredSize(new Dimension(120,40));
			l.setMaximumSize(new Dimension(120,40));
			l.setMinimumSize(new Dimension(120,40));
			l.setHorizontalTextPosition(SwingConstants.LEFT);
			container.add(l);
			container.add(stockLabels[i]);
			container.setVisible(true);
			add(container);
			add(Box.createRigidArea(new Dimension(200,15)));
			
		}

		setVisible(true);
			
	}
	
	
	public void setLabel(int i,String s){
		
		stockLabels[i].setText(s);
		revalidate();
		repaint();
		
	}

	public void setMoney(String s) {
		money.setText(s);
		revalidate();
		repaint();
		
	}
	
	
}
