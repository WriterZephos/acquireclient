package main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class TileSetPanel extends JLabel {

	HashSet<GridButton> buttons;
	BoardPanel board;
	Application app;
	
	public TileSetPanel(BoardPanel board, Application app){
		this.board = board;
		this.app = app;
		init();
		
	}
	
	private void init(){
		
		setPreferredSize(new Dimension(600,200));
		setMaximumSize(new Dimension(600,200));
		setMinimumSize(new Dimension(600,200));
		setLayout(new FlowLayout());
		
		buttons = new HashSet<>();
	
		setOpaque(true);
		setVisible(true);
	}
	
	public void addTile(Tile t){
		
		GridButton b = new GridButton(t);
		b.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				if(!board.isOccupied(t.getX(), t.getY())){
					
					app.getController().placeTile(t);
					buttons.remove(b);
					remove(b);
					revalidate();
					repaint();
				}
				
			}});
		
		add(b);
		revalidate();
		repaint();
		
	}
	
	
	public void reset(){
		
		removeAll();
		
	}

	
	
	
	
	
	
	
	
	
}
