package messages;

import java.util.HashMap;

import org.json.JSONObject;

import main.Tile;

public class MessageFactory {
	
	public enum MajorTypes{
		STOCK,MONEY,BOARD,GAME
	}
	
	public enum StockType{
		BUY,SELL,TRADE
	}
	
	public enum Company{
		LUXOR, TOWER, AMERICAN, WORLDWIDE, FESTIVAL, IMPERIAL, CONTINENTAL, NOT_SET, BLANK
	}
	
	public enum BoardType{
		TILEPLACED,MERGE,CREATE,DRAWTILE
	}
	
	public enum Game{
		JOIN,STARTGAME,ENDTURN,TURN,CONFIRM,DENY
	}
	
	public static JSONObject createInternalChatMessage(String text, String username){
		
		HashMap<String,String> fields = new HashMap<String,String>();
		fields.put("chat", text);
		return JSONMaker.makeApplicationMessage("chat", fields, username);

	}
	
	public static JSONObject createTestMessage(String username){
		
		HashMap<String,String> fields = new HashMap<String, String>();
		return JSONMaker.makeApplicationMessage("test", fields, username);
		
	}

	public static JSONObject createStockMessage(StockType t, Company c,int amount, String username){
		
		HashMap<String,String> fields = new HashMap<String, String>();
		fields.put("option", t.toString());
		fields.put("company", c.toString());
		fields.put("amount", "" + amount);
		
		return JSONMaker.makeApplicationMessage(MajorTypes.STOCK.toString(), fields, username);
		
	}
	
	
	public static JSONObject createTilePlacedMessage(Tile tl, String username){
		
		HashMap<String,String> fields = new HashMap<String, String>();
		fields.put("option", BoardType.TILEPLACED.toString());
		fields.put("tile", tl.toString());
		
		return JSONMaker.makeApplicationMessage(MajorTypes.BOARD.toString(), fields, username);
		
	}
	
	public static JSONObject createMergeMessage(Tile t1, Tile t2_defunct, String summary, String username){
		
		HashMap<String,String> fields = new HashMap<String, String>();
		fields.put("option", BoardType.MERGE.toString());
		fields.put("tile", t1.toString());
		fields.put("tile_defunct", t2_defunct.toString());
		fields.put("summary", summary);
		
		return JSONMaker.makeApplicationMessage(MajorTypes.BOARD.toString(), fields, username);
		
	}
	
	public static JSONObject createCreateBroadcast(Tile t1, Tile t2, Company c, String summary, String username){
		
		HashMap<String,String> fields = new HashMap<String, String>();
		fields.put("option", BoardType.CREATE.toString());
		fields.put("tile1", t1.toString());
		fields.put("tile2", t2.toString());
		fields.put("company", c.toString());
		fields.put("summary", summary);
		
		return JSONMaker.makeApplicationMessage(MajorTypes.BOARD.toString(), fields, username);
		
	}

	
	
	public static JSONObject createStartGameMessage(String username){
		HashMap<String,String> fields = new HashMap<String, String>();
		fields.put("option", Game.STARTGAME.toString());
		return JSONMaker.makeApplicationMessage(MajorTypes.GAME.toString(), fields, username);
		
	}
	
	public static JSONObject createJoinMessage(String username){
		HashMap<String,String> fields = new HashMap<String, String>();
		fields.put("option", Game.JOIN.toString());
		return JSONMaker.makeApplicationMessage(MajorTypes.GAME.toString(), fields, username);
	}
	
	
}
