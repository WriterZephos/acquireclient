package messages;

import java.util.HashMap;
import org.json.JSONObject;

public class MessageParser {
	
	
	public static Update parseUpdate(JSONObject updateObject){
		
		Update u = null;
		
		
		if(updateObject.has("message") && updateObject.get("message") != null){
			
			
			JSONObject message = updateObject.getJSONObject("message");
			JSONObject update = message.getJSONObject("update");
			JSONObject players = update.getJSONObject("players");
			String board = update.getString("board");
			
			HashMap<String,HashMap<String,String>> playersMap = new HashMap<>();
			
			for(String s : JSONObject.getNames(players)){
				
				HashMap<String,String> pMap = new HashMap<>();
				
				JSONObject o = players.getJSONObject(s);
				
				for(String s2 : JSONObject.getNames(o)){
					
					String s3 = o.getString(s2);
					
					pMap.put(s2, s3);
					
				}
				
				playersMap.put(s, pMap);

			}
			
			u = new Update(playersMap,board);

		}
		
		return u;
	}
	
	
	public static class Update{
		
		
		HashMap<String,HashMap<String,String>> playersMap;
		String board;
		
		public Update(HashMap<String,HashMap<String,String>> playersMap,String board){
			this.playersMap = playersMap;
			this.board = board;
			
		}

		public HashMap<String, HashMap<String, String>> getPlayersMap() {
			return playersMap;
		}

		public String getBoard() {
			return board;
		}	
	}
}
