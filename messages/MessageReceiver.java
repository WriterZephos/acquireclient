package messages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;

import org.json.JSONObject;

/**
 * Created by Bryant on 7/2/16.
 * Modified for this project on 8/8/16.
 */
public class MessageReceiver implements Runnable {

    BlockingQueue<String> inputQ;
    BufferedReader reader;

    public MessageReceiver(Socket socket, BlockingQueue<String> inputQ){

        try {
            //initializein input stream
            this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //initialize output message queue
            this.inputQ = inputQ;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    
    @Override
    public void run() {

        //Received message
    	@SuppressWarnings("unused")
		JSONObject m;

        //object to receive
        String inMessage = null;

            //enter the listening loop
        while (!Thread.currentThread().isInterrupted()) {

            //Getting the JSONObject

            //reading new object (blocks thread)
            try {
                inMessage = reader.readLine();
            } catch (IOException e) {
                Thread.currentThread().interrupt();
                break;
            }

            //process the message 
    
            inputQ.add(inMessage);

        }

        try {
        	reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
