package messages;


import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import org.json.JSONObject;

/**
 * Created by Bryant on 7/2/16.
 * Modified for this project on 8/8/16.
 */
public class MessageSender implements Runnable {

    BlockingQueue<JSONObject> outputQ;
    PrintWriter writer;

    public MessageSender(Socket socket, BlockingQueue<JSONObject> outputQ){


        try {
            //initializein output stream and send first message
            this.writer = new PrintWriter(socket.getOutputStream());
            //initialize output message queue
            this.outputQ = outputQ;

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {

        //send messages here
		while(!Thread.currentThread().isInterrupted()){

			JSONObject outMessage = null;
		    try {

		        outMessage = outputQ.take();

		        writer.println(outMessage.toString());
		        writer.flush();

		    } catch (InterruptedException e) {

		        //if interupted while calling take(), re-interrupt (because it clears when the exception is thrown) and break out of loop
		        Thread.currentThread().interrupt();
		        break;
		    }

		}

		writer.close();
    }
}
